import { Component, ViewChildren, ElementRef, QueryList, ViewChild } from '@angular/core';
import { ModalController, IonCard, IonContent } from '@ionic/angular';
import { createAnimation } from '@ionic/core';
import { ModalPage } from '../modal/modal.page';
import { modalEnterAnimation } from '../modal-animation';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public readonly images = [
    'assets/img/photo1.jpg',
    'assets/img/photo2.jpg',
    'assets/img/photo3.jpg',
    'assets/img/photo4.jpg'
  ];

  @ViewChildren('imageElement', { read: ElementRef })
  private imageElements: QueryList<ElementRef<HTMLElement>>;

  @ViewChildren(IonCard, {read: ElementRef})
  private cardElements: QueryList<ElementRef<HTMLElement>>;

  @ViewChild(IonContent, { static: true, read: ElementRef })
  private contentElement: ElementRef<HTMLElement>;

  constructor(
    private modalCtrl: ModalController,
    private pageRef: ElementRef<HTMLElement>
  ) {}

  public async openModal(index: number) {

    const resetAnimation = await this.playAnimation(index);

    const modal = await this.modalCtrl.create({
      component: ModalPage,
      componentProps: { 
        image: this.images[index]
      },
      enterAnimation: modalEnterAnimation
    });
    await modal.present();

    resetAnimation();
  }

  private async playAnimation(index: number) {

    const imageElement = this.imageElements.toArray()[index].nativeElement;
    const firstRect = imageElement.getBoundingClientRect();

    const clone = imageElement.cloneNode(true) as HTMLElement;
    this.pageRef.nativeElement.appendChild(clone);
    clone.classList.add('last-pos');
    const lastRect = clone.getBoundingClientRect();

    const invert = {
      translateX: firstRect.left - lastRect.left,
      translateY: firstRect.top - lastRect.top,
      scaleX: firstRect.width / lastRect.width,
      scaleY: firstRect.height / lastRect.height
    };

    const imageAnimation = createAnimation()
    .addElement(clone)
    .beforeStyles({
      'transform-origin': '0 0'
    })
    .fromTo(
      'transform',
      `translate(${invert.translateX}px, ${invert.translateY}px) scale(${invert.scaleX}, ${invert.scaleY})`,
      'translate(0, 0) scale(1, 1)'
    );

    const cardElement = this.cardElements.toArray()[index].nativeElement;
    cardElement.classList.add('hidden');

    const contentAnimation = createAnimation()
    .addElement(this.contentElement.nativeElement)
    .fromTo(
      'transform',
      'translateX(0)',
      `translateX(-${this.contentElement.nativeElement.offsetWidth}px)`
    );

    const parentAnimation = createAnimation()
    .duration(300)
    .easing('ease-in-out')
    .addAnimation([imageAnimation, contentAnimation]);

    await parentAnimation.play();

    return () => {
      clone.remove();
      cardElement.classList.remove('hidden');
      parentAnimation.stop();
    };
  }
}
